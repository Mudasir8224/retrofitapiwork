package com.example.retrofitpost;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiInterface {

     String baseURL = "https://reqres.in";

    @GET("/api/unknown/2")
    Call<User> getUser();


    @GET("/api/users?page=1")
    Call<ApiResponse<User>> getUsers();


    @POST("/api/users")
    Call<User> createUser(@Body User user);

    @PUT("/api/users/2/{id}")
    Call <User> putUser(@Path("id") String id,@Body User user);


}
