package com.example.retrofitpost;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse<T> {

    @SerializedName("data")
    List<T> userList;

    public List<T> getUserList() {
        return userList;
    }

    public void setUserList(List<T> userList) {
        this.userList = userList;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "userList=" + userList +
                '}';
    }
}
