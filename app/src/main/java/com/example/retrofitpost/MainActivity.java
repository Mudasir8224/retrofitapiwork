package com.example.retrofitpost;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        Call<ApiResponse<User>> call = apiInterface.getUsers();

        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                Log.d("CALL : ", "onResponse: " + response.body().toString());
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                Log.d("CALL", "onFailure: " + t.getMessage());
            }
        });

//         final User user = new User("harron","marketing","22","2june");
//        Call<User> call1 = apiInterface.createUser(user);
//        call1.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Call<User> call, Response<User> response) {
//                User user1 = response.body();
//
//                Log.d("name",user1.name);
//                Log.d("id",user1.id);
//                Log.d("createdat",user1.createdAt);
//
//                Toast.makeText(getApplicationContext(), user1.name + " " + user1.job + " " + user1.id + " " + user1.createdAt, Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFailure(Call<User> call, Throwable t) {
//                call.cancel();
//            }
//        });

    }
/*
   private void updateUser (){
        User user = new User("ahmed","cc","77","bokef");

        final Call<User> userCall = apiInterface.putUser("88",user);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user2 = response.body();
                Log.d("name",user2.name);
                Log.d("job",user2.id);
                Log.d("id",user2.id);
                Log.d("createdat",user2.createdAt);

                Toast.makeText(getApplicationContext(), user2.name + " " + user2.job + " " + user2.id + " " + user2.createdAt, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                call.cancel();

            }

        });

 */

   /*  private  void getUser(){

         Call<List<User>> call = apiInterface.getUsers();

         call.enqueue(new Callback<List<User>>() {
             @Override
                 public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                 List<User> heroes = response.body();

                 for(User h: heroes){
                     Log.d("createdby",h.getName());
                     Log.d("team",h.getJob());
                     Log.d("realname",h.getId());

                 }
             }

             @Override
             public void onFailure(Call<List<User>> call, Throwable t) {

                 Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();

             }
         });

    */


}
